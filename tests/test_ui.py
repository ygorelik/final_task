import allure
from tools.work_with_DB.db_requests import create_table, check_data_base
from tools.page_object.main_page import MainPage


@allure.feature("Test UI")
def test_check_group(driver):
    """
    Тест, что искомая группа отображается во вкладке Groups
    """
    create_table()
    main_page = MainPage(driver)
    main_page.open()
    login_page = main_page.open_login_page()
    login_page.open()
    admin_page = login_page.open_admin_page()
    admin_page.open()
    groups_page = admin_page.open_groups_page()
    groups_page.open()
    with allure.step("Test check group"):
        assert groups_page.get_name_group().text == "new_group", \
            "Group not found!"


@allure.feature("Test UI")
def test_check_user(driver):
    """
    # Тест, что в базе данных созданный пользователь добавлен в указанную
    # группу
    """
    main_page = MainPage(driver)
    main_page.open()
    login_page = main_page.open_login_page()
    login_page.open()
    admin_page = login_page.open_admin_page()
    admin_page.open()
    create_new_user_page = admin_page.create_new_user()
    create_new_user_page.open()
    add_new_user_page = create_new_user_page.add_new_user()
    add_new_user_page.open()
    users_page = add_new_user_page.add_users_to_group()
    users_page.open()
    check_data_base()
    with allure.step("Test check user"):
        assert users_page.get_name_user().text == check_data_base(), \
            "User not found!"
