import pytest
from selenium import webdriver


@pytest.fixture(scope="function")
def driver():
    """
    Фикстура старта и закрытия драйвера
    :return: драйвер
    """
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--headless')
    chrome_options.add_argument('--disable-gpu')
    driver = webdriver.Chrome('/usr/local/bin/chromedriver',
                              chrome_options=chrome_options)
    driver.maximize_window()
    driver.implicitly_wait(10)
    yield driver
    driver.quit()
