import allure

from tools.api_requests.requests_api import get_data_user, get_json, \
    create_new_user, login_new_user, logout_user, delete_user


@allure.feature("Test API")
def test_create_new_user():
    """
    Тест, что пользователь был создан
    """
    new_user = get_json()
    response = create_new_user(new_user)
    with allure.step("Test create new user"):
        assert response.status_code == 200, "Failed to request"


@allure.feature("Test API")
def test_login():
    """
    Тест, что пользователь может войти в свою учётную запись
    """
    new_user = get_json()
    response = login_new_user(new_user)
    with allure.step("Test login user"):
        assert response.status_code == 200, "Failed to request"


@allure.feature("Test API")
def test_get_data_user():
    """
    Тест, что пользователь получает данные о себе
    """
    new_user = get_json()
    user_name = new_user['username']
    response = get_data_user(user_name)
    response_body = response.json()
    with allure.step("Test view user data"):
        assert response_body["email"] == new_user['email']


@allure.feature("Test API")
def test_logout_user():
    """
    Тест, что пользователь может выйти из своей учётной записи
    """
    response = logout_user()
    response_body = response.json()
    with allure.step("Test logout user"):
        assert response_body["message"] == "ok"


@allure.feature("Test API")
def test_delete_user():
    """
    Тест, что пользователь может удалить свою учётную запись
    """
    new_user = get_json()
    user_name = new_user['username']
    response = delete_user(user_name)
    with allure.step("Test delete user"):
        assert response.status_code == 200, "Failed to request"
