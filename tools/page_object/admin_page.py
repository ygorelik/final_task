from tools.page_object.base_page import BasePage
from tools.page_object.create_user_page import CreateUserPage
from tools.page_object.groups_page import GroupsPage
from tools.page_locators.admin_page_locators import AdminPageLocators


class AdminPage(BasePage):
    """
    Класс страницы admin
    """

    def open_groups_page(self):
        """
        Функция открытия страницы groups
        """
        groups_link = self.find_element(AdminPageLocators.GROUPS_LOCATORS)
        groups_link.click()
        return GroupsPage(self.driver, self.driver.current_url)

    def create_new_user(self):
        """
        Функция открытия страницы создания нового пользователя
        """
        add_link = self.find_element(AdminPageLocators.ADD_USER_LOCATORS)
        add_link.click()
        return CreateUserPage(self.driver, self.driver.current_url)
