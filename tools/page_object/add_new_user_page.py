from tools.page_object.base_page import BasePage
from tools.page_object.users_page import UsersPage
from tools.page_locators.add_new_user_page_locators import \
    AddNewUserPageLocators


class AddNewUserPage(BasePage):
    """
    Класс работы с рользователем
    """

    def add_users_to_group(self):
        """
        Функция добавления пользователя в группу
        """
        choose_group = self.find_element(
            AddNewUserPageLocators.SELECTOR_ADD_USER)
        choose_group.click()
        save_button = self.find_element(AddNewUserPageLocators.SAVE_BUTTON)
        save_button.click()
        return UsersPage(self.driver, self.driver.current_url)
