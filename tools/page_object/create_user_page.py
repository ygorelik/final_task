from tools.page_object.add_new_user_page import AddNewUserPage
from tools.page_object.base_page import BasePage
from tools.page_locators.create_user_page_locators import \
    CreateUserPageLocators


class CreateUserPage(BasePage):
    """
    Класс страницы создания нового пользователя
    """

    def add_new_user(self):
        """
        Функция создания нового пользователя и перехода на старницу
        пользователей
        """
        username_input = self.find_element(
            CreateUserPageLocators.INPUT_FIELD_USERNAME_LOCATORS)
        username_input.send_keys("Yuri_Gorelik")
        password_input = self.find_element(
            CreateUserPageLocators.INPUT_FIELD_PASSWORD_LOCATORS)
        password_input.send_keys("TMS_2021")
        password_confirmation_input = self.find_element(
            CreateUserPageLocators.INPUT_FIELD_PASSWORD_CONFIRMATION_LOCATORS)
        password_confirmation_input.send_keys("TMS_2021")
        save_button = self.find_element(CreateUserPageLocators.SAVE_BUTTON)
        save_button.click()
        return AddNewUserPage(self.driver, self.driver.current_url)
