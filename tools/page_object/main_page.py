from tools.page_object.base_page import BasePage
from tools.page_object.login_page import LoginPage
from tools.page_locators.main_page_locators import MainPageLocators


class MainPage(BasePage):
    """
    Класс главной страницы сайта
    """
    URL = "http://host.docker.internal:8000"

    def __init__(self, driver):
        super().__init__(driver, self.URL)

    def open_login_page(self):
        """
        Функция перехода на страницу логина
        """
        login_link = self.find_element(MainPageLocators.ADMIN_LOCATORS)
        login_link.click()
        return LoginPage(self.driver, self.driver.current_url)
