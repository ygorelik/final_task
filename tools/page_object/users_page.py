from tools.page_object.base_page import BasePage
from tools.page_locators.users_page_locators import UsersPageLocators


class UsersPage(BasePage):
    """
    Класс страницы users
    """

    def get_name_user(self):
        """
        Функция выбора необходимого пользователя
        """
        return self.find_element(UsersPageLocators.CHOOSE_USER_LOCATORS)
