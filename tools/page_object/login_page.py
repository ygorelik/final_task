from tools.page_object.admin_page import AdminPage
from tools.page_object.base_page import BasePage
from tools.page_locators.login_page_locators import LoginPageLocators


class LoginPage(BasePage):
    """
    Класс страницы log_in
    """

    def open_admin_page(self):
        """
        Функция перехода на страницы admin
        """
        username_input = self.find_element(LoginPageLocators.
                                           INPUT_FIELD_USERNAME_LOCATORS)
        username_input.send_keys("admin")
        password_input = self.find_element(LoginPageLocators.
                                           INPUT_FIELD_PASSWORD_LOCATORS)
        password_input.send_keys("password")
        log_in_button = self.find_element(LoginPageLocators.
                                          LOG_IN_BUTTON_LOCATORS)
        log_in_button.click()
        return AdminPage(self.driver, self.driver.current_url)
