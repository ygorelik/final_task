from tools.page_object.base_page import BasePage
from tools.page_locators.groups_page_locators import GroupsPageLocators


class GroupsPage(BasePage):
    """
    Класс страницы admin
    """

    def get_name_group(self):
        """
        Функция определения существования необходимой группы
        """
        return self.find_element(GroupsPageLocators.GROUP_LOCATORS)
