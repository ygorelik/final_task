from selenium.webdriver.common.by import By


class AddNewUserPageLocators:
    """
    Класс локаторов страницы настроек пользователя
    """
    SELECTOR_ADD_USER = (
        By.XPATH, '//a[@title="Click to choose all groups at once."]')
    SAVE_BUTTON = (By.CSS_SELECTOR, '.submit-row>.default')
