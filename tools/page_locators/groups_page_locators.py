from selenium.webdriver.common.by import By


class GroupsPageLocators:
    """
    Класс локаторов страницы groups
    """
    GROUP_LOCATORS = (By.XPATH, '//a[@href="/admin/auth/group/2/change/"]')
