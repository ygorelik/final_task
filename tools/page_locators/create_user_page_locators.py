from selenium.webdriver.common.by import By


class CreateUserPageLocators:
    """
    Класс локаторов страницы создания нового пользователя
    """
    INPUT_FIELD_USERNAME_LOCATORS = (By.CSS_SELECTOR, '#id_username')
    INPUT_FIELD_PASSWORD_LOCATORS = (By.CSS_SELECTOR, '#id_password1')
    INPUT_FIELD_PASSWORD_CONFIRMATION_LOCATORS = (
        By.CSS_SELECTOR, '#id_password2')
    SAVE_BUTTON = (By.CSS_SELECTOR, '.submit-row>.default')
