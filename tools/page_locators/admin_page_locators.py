from selenium.webdriver.common.by import By


class AdminPageLocators:
    """
    Класс локаторов страницы admin
    """
    GROUPS_LOCATORS = (By.CSS_SELECTOR, '.model-group .changelink')
    ADD_USER_LOCATORS = (By.XPATH, '//a[@href="/admin/auth/user/add/"]')
