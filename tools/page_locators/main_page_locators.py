from selenium.webdriver.common.by import By


class MainPageLocators:
    """
    Класс локаторов главной страницы
    """
    ADMIN_LOCATORS = (By.XPATH, '//a[@href="/admin"]')
