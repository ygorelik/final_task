from selenium.webdriver.common.by import By


class LoginPageLocators:
    """
    Класс локаторов страницы log_in
    """
    INPUT_FIELD_USERNAME_LOCATORS = (By.CSS_SELECTOR, '#id_username')
    INPUT_FIELD_PASSWORD_LOCATORS = (By.CSS_SELECTOR, '#id_password')
    LOG_IN_BUTTON_LOCATORS = (By.XPATH, '//input[@type="submit"]')
