from selenium.webdriver.common.by import By


class UsersPageLocators:
    """
    Класс локаторов страницы пользователей
    """
    CHOOSE_USER_LOCATORS = (
        By.XPATH, '(//th[@class="field-username"])[2]')
