import psycopg2

conn = psycopg2.connect(dbname='postgres',
                        user='postgres',
                        password='postgres',
                        host='host.docker.internal')


def create_table():
    """
    Функция добавления новой группы в базу данных
    """
    cursor = conn.cursor()
    # отправка запроса на добавление новой группы с id ранным 2 и
    # названием "new_group"
    cursor.execute(
        "INSERT INTO auth_group (id, name) VALUES(2, 'new_group');"
    )
    conn.commit()
    cursor.close()


def check_data_base():
    """
    Функция проверки добавления пользователя в нужную группу
    :return: первого пользователя из выборки, который отвечает критериям
    запроса
    """
    cursor = conn.cursor()
    # Запрос, который возвращает имена пользователей из таблицы "auth_user",
    # которые добавленые в группу с id равным 2
    query = """
    SELECT auth_user.username AS name_user
    FROM auth_user
    JOIN auth_user_groups
    ON auth_user.id = auth_user_groups.user_id
    WHERE group_id = 2
    """
    cursor.execute(query)
    name_user = cursor.fetchone()
    cursor.close()
    return name_user[0]
