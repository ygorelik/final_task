import json
import os

import requests

link = "https://petstore.swagger.io/v2/"


def create_new_user(content_json: dict):
    """
    Функция создания нового пользователя
    :return: ответ с сервера с данными
    """
    response = requests.post(f"{link}user", json=content_json)
    return response


def login_new_user(content_json: dict):
    """
    Функция логина нового пользователя
    :return: ответ с сервера с данными
    """
    response = requests.get(f"{link}user/login", json=content_json)
    return response


def get_data_user(username: str):
    """
    Функция получения данных о пользователе
    :return: ответ с сервера с данными
    """
    response = requests.get(f"{link}user/{username}")
    return response


def logout_user():
    """
    Функция логаута пользователя
    :return: ответ с сервера с данными
    """
    response = requests.get(f"{link}user/logout")
    return response


def delete_user(username: str):
    """
    Функция удаления пользователя
    :return: ответ с сервера с данными
    """
    response = requests.delete(f"{link}user/{username}")
    return response


def get_json():
    """
    Функция получения содержимого json файла new_user
    :return: словарь содержимого файла new_user
    """
    file = os.getcwd()
    file = file.replace("tests", "") + "/tools/api_requests/new_user.json"
    file_obj = open(file, "r")
    content = file_obj.read()
    content_json = json.loads(content)
    file_obj.close()
    return content_json
